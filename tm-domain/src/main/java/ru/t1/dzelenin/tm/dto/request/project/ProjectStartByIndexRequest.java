package ru.t1.dzelenin.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.dto.request.AbstractIndexRequest;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectStartByIndexRequest extends AbstractIndexRequest {

    public ProjectStartByIndexRequest(@Nullable final String token, @Nullable final Integer index) {
        super(token, index);
    }

}


