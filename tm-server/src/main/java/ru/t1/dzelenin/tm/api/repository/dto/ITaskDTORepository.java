package ru.t1.dzelenin.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskDTORepository extends IUserOwnedDTORepository<TaskDTO> {

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeTasksByProjectId(String projectId);

}