package ru.t1.dzelenin.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.dzelenin.tm.api.service.IConnectionService;
import ru.t1.dzelenin.tm.api.service.dto.ITaskDTOService;
import ru.t1.dzelenin.tm.dto.model.TaskDTO;
import ru.t1.dzelenin.tm.exception.field.IdEmptyException;
import ru.t1.dzelenin.tm.exception.field.NameEmptyException;
import ru.t1.dzelenin.tm.exception.field.UserIdEmptyException;
import ru.t1.dzelenin.tm.repository.dto.TaskDTORepository;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

public class TaskDTOService extends AbstractUserOwnedDTOService<TaskDTO, ITaskDTORepository> implements ITaskDTOService {

    public TaskDTOService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected ITaskDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return new TaskDTORepository(entityManager);
    }

    @Nullable
    @Override
    public TaskDTO create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setUserId(userId);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Nullable
    @Override
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Nullable
    @Override
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            return repository.findAllByProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }
    }

}
